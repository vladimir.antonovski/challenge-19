import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Navbar from "./components/navbar";
import ArtistList from "./components/artistList";
import ArtistPage from "./components/artistPage";
import AlbumPage from "./components/albumPage";
import PageNotFound from "./components/pageNotFound";

const App: React.FC = () => {
    return (
        <Router>
            <Navbar />
            <Routes>
                <Route path="/" element={<ArtistList />} />
                <Route path="/artist/:artistId" element={<ArtistPage />} />
                <Route
                    path="/artist/:artistId/:artistAlbum"
                    element={<AlbumPage />}
                />
                <Route path="*" element={<PageNotFound />} />
            </Routes>
        </Router>
    );
};

export default App;
