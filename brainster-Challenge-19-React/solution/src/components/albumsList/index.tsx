import React from "react";
import { Link } from "react-router-dom";
import style from "./style.module.css";

interface albumData {
    albumId: string;
    title: string;
    year: number;
    cover: string;
    price: number;
}

interface Props {
    albums: albumData[];
    id: number;
}

const AlbumsList: React.FC<Props> = ({ albums, id }) => {
    return (
        <div className={style.albumsList}>
            {albums.map((album) => (
                <Link
                    key={album.albumId}
                    to={`/artist/${id}/${album.albumId}`}
                    className={style.albumCover}
                >
                    <img
                        src={require(`../../assets/albums/${album.cover}.jpg`)}
                        alt={`${album.cover}-pic`}
                    />
                </Link>
            ))}
        </div>
    );
};

export default AlbumsList;
