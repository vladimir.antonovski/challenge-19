import React from "react";
import navbarPic from "../../assets/raw/Girls-Listen-Music_0.jpg";
import style from "./style.module.css";
import { Link } from "react-router-dom";

const Navbar: React.FC = () => {
    return (
        <div className={style.navbar}>
            <Link to="/">
                <img
                    src={navbarPic}
                    alt="navbar-pic"
                    className={style.navbarPic}
                />
            </Link>
            <h1>music db</h1>
        </div>
    );
};

export default Navbar;
