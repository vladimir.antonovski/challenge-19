import React from "react";
import style from "./style.module.css";
import { useParams } from "react-router-dom";
import artists from "../../data/db";
import AlbumsList from "../albumsList";

const ArtistPage: React.FC = () => {
    const { artistId } = useParams();
    const currentArtist =
        artistId &&
        artists.filter((artist) => artist.id.toString() === artistId)[0];

    return currentArtist ? (
        <div className={style.artistPage}>
            <img
                src={require(`../../assets/covers/${currentArtist.cover}.jpg`)}
                alt={`${currentArtist.name}-pic`}
                className={style.artistImg}
            />
            <p className={style.artistName}>{currentArtist.name}</p>
            <p className={style.info}>{currentArtist.bio}</p>
            <AlbumsList albums={currentArtist.albums} id={currentArtist.id} />
        </div>
    ) : (
        <></>
    );
};

export default ArtistPage;
