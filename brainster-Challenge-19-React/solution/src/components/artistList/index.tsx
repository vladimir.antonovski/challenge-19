import React from "react";
import style from "./style.module.css";
import artists from "../../data/db";
import { Link } from "react-router-dom";
import ArtistItem from "../artistItem";

const ArtistList: React.FC = () => {
    return (
        <div className={style.artistList}>
            <p className={style.browseTitle}>Browse the artists</p>
            <div className={style.artists}>
                {artists.map((artist) => (
                    <Link
                        key={artist.id}
                        to={`/artist/${artist.id}`}
                        className={style.artistName}
                    >
                        <ArtistItem name={artist.name} cover={artist.cover} />
                    </Link>
                ))}
            </div>
        </div>
    );
};

export default ArtistList;
