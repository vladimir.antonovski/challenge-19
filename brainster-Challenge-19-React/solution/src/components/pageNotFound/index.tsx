import React from "react";
import style from "./style.module.css";

const PageNotFound: React.FC = () => {
    return (
        <div className={style.pageNotFound}>
            <h3> ERROR 404 </h3>
            <p>Page not found</p>
        </div>
    );
};

export default PageNotFound;
