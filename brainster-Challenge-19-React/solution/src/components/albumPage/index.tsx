import React from "react";
import { useParams } from "react-router-dom";
import style from "./style.module.css";
import artists from "../../data/db";

const AlbumPage: React.FC = () => {
    const { artistAlbum } = useParams();
    const { artistId } = useParams();

    const currentArtist =
        artistId &&
        artists.filter((artist) => artist.id.toString() === artistId)[0];

    const currentAlbumCover =
        currentArtist &&
        currentArtist.albums.filter(
            (cover) => cover.albumId === artistAlbum
        )[0];

    return currentAlbumCover ? (
        <div className={style.albumPage}>
            <img
                src={require(`../../assets/albums/${currentAlbumCover.cover}.jpg`)}
                alt={`${currentAlbumCover.cover}`}
                className={style.coverImg}
            />
            <p>
                <b>Title:</b> {currentAlbumCover.title}
            </p>
            <p>
                <b>Year:</b> {currentAlbumCover.year}
            </p>
            <p>
                <b>Price:</b> {currentAlbumCover.price} $
            </p>
        </div>
    ) : (
        <></>
    );
};

export default AlbumPage;
