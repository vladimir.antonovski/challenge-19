import React from "react";
import style from "./style.module.css";

interface Props {
    cover: string;
    name: string;
}

const ArtistItem: React.FC<Props> = ({ cover, name }) => {
    return (
        <div>
            <div className={style.wrapper}>
                <img
                    src={require(`../../assets/covers/${cover}.jpg`)}
                    alt="artist-pic"
                />
                <p>{name}</p>
            </div>
        </div>
    );
};

export default ArtistItem;
